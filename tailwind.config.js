module.exports = {
    theme: {
        extend: {}
    },
    variants: {
        textColor: ['hover', 'focus', 'active'],
        boxShadow: ['hover', 'focus', 'active'],
        backgroundColor: ['hover', 'focus', 'active'],
        border: ['hover', 'focus', 'active']
    },
    plugins: [
    ]
}
