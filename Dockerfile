FROM node:lts-alpine AS builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM nginx:alpine
ENV NGINX_PORT 80
ENV NGINX_HOST _
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/dist/ .
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
