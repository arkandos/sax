const path = require('path')
const autoprefixer = require('autoprefixer')
const tailwindcss = require('tailwindcss')
const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
    plugins: [
        tailwindcss(path.join(__dirname, 'tailwind.config.js')),
        autoprefixer(),

        ...process.env.NODE_ENV === 'production'
            ? [purgecss({
                content: [
                    './src/index.html',
                    './src/**/*.elm'
                ],

                fontFace: true,
                keyframes: true,

                whitelist: ['body', 'html', '*'],

                defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
            })]
            : []
    ]
}
