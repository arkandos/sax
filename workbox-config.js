module.exports = {
    "globDirectory": "dist/",
    "globPatterns": [
        "**/*"
    ],
    "swDest": "dist/service-worker.js",
    "navigateFallback": "/index.html",
    "importWorkboxFrom": "local",
    "ignoreURLParametersMatching": [/^utm_/, /^\d+$/]
}
