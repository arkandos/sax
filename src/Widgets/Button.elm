module Widgets.Button exposing (default, defaultLink)

import Html exposing (Attribute, Html, i, span, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Router exposing (Route)


default : List (Attribute msg) -> msg -> String -> String -> Html msg
default attrs msg icon label =
    buttonContent icon label
        |> Html.button (buttonClasses :: onClick msg :: attrs)


defaultLink : List (Attribute msg) -> Route -> String -> String -> Html msg
defaultLink attrs route icon label =
    buttonContent icon label
        |> Html.a (buttonClasses :: Router.href route :: attrs)


buttonContent : String -> String -> List (Html msg)
buttonContent icon label =
    [ i [ class icon ] []
    , span [ class "font-bold text-lg" ] [ text label ]
    ]


buttonClasses : Attribute msg
buttonClasses =
    class "flex items-center py-1 px-2 text-orange-800 bg-orange-300 rounded shadow transition transition-fast ease-in-out active:bg-orange-800 active:text-orange-200 focus:outline-none"
