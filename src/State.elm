module State exposing
    ( Model
    , Msg
    , count
    , empty
    , favorites
    , favoritesAndSelected
    , init
    , isFavorite
    , lastUpdate
    , persist
    , subscriptions
    , toggleFavorite
    , update
    , updateCount
    )

import Browser.Events exposing (Visibility(..), onVisibilityChange)
import Date exposing (Date, Unit(..))
import Dict.Any as AnyDict exposing (AnyDict)
import Drinks exposing (Drink)
import Json.Decode as Json exposing (Decoder)
import Json.Encode as Encode
import Ports
import Set.Any as AnySet exposing (AnySet)
import Task
import Time exposing (Posix)
import Time.Extra as Time



--
-- INIT
--


type Model
    = Model
        { favorites : AnySet String Drink
        , counts : AnyDict String Drink Int
        , totals : AnyDict String Drink Int
        , history : AnyDict String Date (AnyDict String Drink Int)
        , lastUpdate : Posix
        }


type Msg
    = StateUpdated Json.Value
    | AppActivated Time.Zone Posix
    | VisibilityChanged Visibility


empty : (Msg -> msg) -> ( Model, Cmd msg )
empty toMsg =
    ( Model
        { favorites = AnySet.empty .key
        , counts = AnyDict.empty .key
        , totals = AnyDict.empty .key
        , history = AnyDict.empty Date.toIsoString
        , lastUpdate = Time.millisToPosix 0
        }
    , appActivated toMsg
    )


init : (Msg -> msg) -> Json.Value -> Result Json.Error ( Model, Cmd msg )
init toMsg jsonFlags =
    Json.decodeValue decoder jsonFlags
        |> Result.map (\state -> ( state, appActivated toMsg ))



--
-- QUERY
--


lastUpdate : Model -> Posix
lastUpdate (Model model) =
    model.lastUpdate


favorites : Model -> List Drink
favorites (Model model) =
    AnySet.toList model.favorites


count : Drink -> Model -> Int
count drink (Model model) =
    AnyDict.get drink model.counts
        |> Maybe.withDefault 0


isFavorite : Drink -> Model -> Bool
isFavorite drink (Model model) =
    AnySet.member drink model.favorites


favoritesAndSelected : Model -> List Drink
favoritesAndSelected (Model model) =
    let
        favoritesWithoutSelected =
            AnyDict.keys model.counts
                |> List.foldl AnySet.remove model.favorites
                |> AnySet.toList

        sortedSelected =
            AnyDict.toList model.counts
                |> List.sortBy Tuple.second
                |> List.foldl (\( drink, _ ) result -> drink :: result) []
    in
    sortedSelected ++ favoritesWithoutSelected



--
-- UPDATE
--


toggleFavorite : Drink -> Model -> Model
toggleFavorite drink (Model model) =
    let
        operation =
            if isFavorite drink (Model model) then
                AnySet.remove

            else
                AnySet.insert
    in
    Model { model | favorites = operation drink model.favorites }


updateCount : Drink -> Int -> Model -> Model
updateCount drink diff (Model model) =
    let
        getNewCount current =
            let
                updated =
                    Basics.max 0 <| diff + Maybe.withDefault 0 current
            in
            if updated > 0 then
                Just updated

            else
                Nothing
    in
    Model
        { model
        | counts = AnyDict.update drink getNewCount model.counts
        , totals = AnyDict.update drink getNewCount model.totals
        }


update : (Msg -> msg) -> Msg -> Model -> ( Model, Cmd msg )
update toMsg msg (Model model) =
    case msg of
        StateUpdated jsonState ->
            ( Json.decodeValue decoder jsonState
                |> Result.withDefault (Model model)
            , Cmd.none
            )

        VisibilityChanged visibility ->
            if visibility == Visible then
                ( Model model, appActivated toMsg )

            else
                ( Model model, Cmd.none )

        AppActivated zone time ->
            let
                todayAt3pm =
                    Time.posixToMillis <| at3pm zone time
            in
            if Time.posixToMillis model.lastUpdate < todayAt3pm && Time.posixToMillis time >= todayAt3pm then
                let
                    lastUpdateAt3pm =
                        Time.posixToMillis <| at3pm zone model.lastUpdate

                    dateToRecord =
                        if Time.posixToMillis model.lastUpdate < lastUpdateAt3pm then
                            -- if the last update is before 3pm, I assume you just went...
                            -- a little overboard the night before
                            Date.fromPosix zone model.lastUpdate
                                |> Date.add Days -1

                        else
                            Date.fromPosix zone model.lastUpdate
                in
                ( Model
                    { model
                        | counts = AnyDict.empty .key
                        -- TODO: At some point, we should remove old history entries,
                        --       but I'm not quite sure after how long that would be
                        , history = AnyDict.insert dateToRecord model.counts model.history
                        , lastUpdate = time
                    }
                , Cmd.none
                )

            else
                ( Model model, Cmd.none )



at3pm : Time.Zone -> Posix -> Posix
at3pm zone =
    Time.floor Time.Day zone >> Time.add Time.Hour 15 zone


--
-- COMMANDS
--


persist : Model -> Cmd msg
persist state =
    Ports.persistState <| encode state


appActivated : (Msg -> msg) -> Cmd msg
appActivated toMsg =
    Task.map2 AppActivated Time.here Time.now
        |> Task.perform toMsg



--
-- SUBSCRIPTIONS
--


subscriptions : (Msg -> msg) -> Sub msg
subscriptions toMsg =
    Sub.batch
        [ Ports.stateUpdated (StateUpdated >> toMsg)
        , onVisibilityChange (VisibilityChanged >> toMsg)
        ]



--
-- DECODER
--


decoder : Decoder Model
decoder =
    Json.map5
        (\favs counts totals history time ->
            Model
                { favorites = favs
                , counts = counts
                , totals = Maybe.withDefault (AnyDict.empty .key) totals
                , history = Maybe.withDefault (AnyDict.empty Date.toIsoString) history
                , lastUpdate = Maybe.withDefault (Time.millisToPosix 0) time
                }
        )
        (Json.field "favorites" <| anySetDecoder Json.string Drinks.byKey .key)
        (Json.field "counts" countsDecoder)
        (Json.maybe <| Json.field "totals" countsDecoder)
        (Json.maybe <| Json.field "history" historyDecoder)
        (Json.maybe <| Json.field "lastUpdate" <| Json.map Time.millisToPosix Json.int)


historyDecoder : Decoder (AnyDict String Date (AnyDict String Drink Int))
historyDecoder =
    anyDictDecoder countsDecoder (Date.fromIsoString >> Result.toMaybe) Date.toIsoString


countsDecoder : Decoder (AnyDict String Drink Int)
countsDecoder =
    anyDictDecoder Json.int Drinks.byKey .key


anySetDecoder : Decoder comparable -> (comparable -> Maybe v) -> (v -> comparable) -> Decoder (AnySet comparable v)
anySetDecoder itemDecoder getKey toComparable =
    Json.list itemDecoder
        |> Json.map (List.filterMap getKey)
        |> Json.map (AnySet.fromList toComparable)


anyDictDecoder : Decoder v -> (String -> Maybe k) -> (k -> String) -> Decoder (AnyDict String k v)
anyDictDecoder itemDecoder getKey toComparable =
    let
        getPair ( comparable, value ) =
            getKey comparable
                |> Maybe.map (\key -> ( key, value ))
    in
    Json.keyValuePairs itemDecoder
        |> Json.map (List.filterMap getPair)
        |> Json.map (AnyDict.fromList toComparable)


--
-- ENCODER
--


encode : Model -> Json.Value
encode (Model model) =
    Encode.object
        [ ( "favorites", encodeAnySet Encode.string model.favorites )
        , ( "counts", encodeAnyDict Encode.int model.counts )
        , ( "totals", encodeAnyDict Encode.int model.totals )
        , ( "history", encodeAnyDict (encodeAnyDict Encode.int) model.history )
        , ( "lastUpdate", encodePosix model.lastUpdate )
        ]


encodePosix : Posix -> Json.Value
encodePosix =
    Time.posixToMillis >> Encode.int


encodeAnySet : (comparable -> Json.Value) -> AnySet comparable item -> Json.Value
encodeAnySet encodeItem =
    AnySet.toSet >> Encode.set encodeItem


encodeAnyDict : (v -> Json.Value) -> AnyDict String k v -> Json.Value
encodeAnyDict encodeItem =
    AnyDict.toDict >> Encode.dict identity encodeItem
