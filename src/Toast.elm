module Toast exposing
    ( Model
    , Msg
    , Toast
    , action
    , actionWithDuration
    , empty
    , init
    , map
    , notification
    , push
    , pushAll
    , update
    , view
    )

import Html exposing (Html, button, div, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Utils



--
-- CONFIG
--


type alias ToastData msg =
    { message : String
    , action : Maybe ( msg, String )
    , duration : Maybe Float
    }


type Toast msg
    = Toast (ToastData msg)


notification : Float -> String -> Toast msg
notification duration message =
    Toast
        { message = message
        , action = Nothing
        , duration = Just duration
        }


action : String -> msg -> String -> Toast msg
action message actionMsg actionLabel =
    Toast
        { message = message
        , action = Just ( actionMsg, actionLabel )
        , duration = Nothing
        }


actionWithDuration : Float -> String -> msg -> String -> Toast msg
actionWithDuration duration message actionMsg actionLabel =
    Toast
        { message = message
        , action = Just ( actionMsg, actionLabel )
        , duration = Just duration
        }


map : (msg1 -> msg2) -> Toast msg1 -> Toast msg2
map mapMsg (Toast toast) =
    Toast
        { message = toast.message
        , duration = toast.duration
        , action = Maybe.map (Tuple.mapFirst mapMsg) toast.action
        }



--
-- INIT
--


type Model msg
    = Model
        { toasts : List ( Int, ToastData msg )
        , id : Int
        }


empty : Model msg
empty =
    Model
        { toasts = []
        , id = 0
        }


init : ( Model msg, Cmd msg )
init =
    ( empty, Cmd.none )



--
-- UPDATE
--


type Msg
    = Dismissed Int
    | ActionClicked Int


push : (Msg -> msg) -> Model msg -> Toast msg -> ( Model msg, Cmd msg )
push toMsg (Model model) (Toast toast) =
    let
        id =
            model.id

        newModel =
            Model
                { toasts = List.append model.toasts [ ( id, toast ) ]
                , id = id + 1
                }
    in
    case toast.duration of
        Just duration ->
            ( newModel, Utils.delay duration <| toMsg (Dismissed id) )

        Nothing ->
            ( newModel, Cmd.none )


pushAll : (Msg -> msg) -> Model msg -> List (Toast msg) -> ( Model msg, Cmd msg )
pushAll toMsg model toasts =
    Tuple.mapSecond Cmd.batch <|
        List.foldl
            (\toast ( curModel, cmds ) ->
                push toMsg curModel toast
                    |> Tuple.mapSecond (\cmd -> cmd :: cmds)
            )
            ( model, [] )
            toasts


update : Model msg -> Msg -> ( Model msg, Cmd msg )
update (Model model) msg =
    case ( msg, model.toasts ) of
        ( Dismissed msgId, ( toastId, _ ) :: rest ) ->
            if msgId == toastId then
                ( Model { model | toasts = rest }, Cmd.none )

            else
                ( Model model, Cmd.none )

        ( Dismissed _, [] ) ->
            ( Model model, Cmd.none )

        ( ActionClicked msgId, ( toastId, toast ) :: rest ) ->
            let
                newModel =
                    Model { model | toasts = rest }
            in
            if msgId == toastId then
                case toast.action of
                    Just ( actionMsg, _ ) ->
                        ( newModel, Utils.delay 0 actionMsg )

                    Nothing ->
                        ( newModel, Cmd.none )

            else
                ( newModel, Cmd.none )

        ( ActionClicked _, [] ) ->
            ( Model model, Cmd.none )



--
-- VIEW
--


view : (Msg -> msg) -> Model msg -> Html msg
view toMsg (Model model) =
    case List.head model.toasts of
        Just ( toastId, toast ) ->
            div [ class "fixed inset-x-0 bottom-0 flex justify-between p-3 bg-orange-900 text-white" ]
                [ text toast.message
                , case toast.action of
                    Just ( _, actionLabel ) ->
                        button
                            [ onClick <| toMsg (ActionClicked toastId)
                            , class "font-bold text-orange-500"
                            ]
                            [ text actionLabel ]

                    Nothing ->
                        text ""
                ]

        Nothing ->
            text ""
