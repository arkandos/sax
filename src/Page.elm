module Page exposing (Model, Msg, fromRoute, route, update, view)

import Browser.Navigation as Nav
import Html exposing (Html)
import Pages.Drinks as DrinksPage
import Pages.Impressum as ImpressumPage
import Pages.InternalError as InternalErrorPage
import Pages.NotFound as NotFoundPage
import Pages.Start as StartPage
import Router exposing (Route(..))
import State



--
-- INIT
--


type Page
    = Static
    | Drinks DrinksPage.Model


type alias Model =
    { route : Route
    , page : Page
    }


fromRoute : Route -> State.Model -> Model
fromRoute theRoute state =
    let
        page =
            case theRoute of
                Router.Drinks category ->
                    Drinks <| DrinksPage.initForCategory category

                Router.Favorites ->
                    Drinks <| DrinksPage.initForFavorites state

                _ ->
                    Static
    in
    Model theRoute page


route : Model -> Route
route model =
    model.route



--
-- UPDATE
--


type Msg
    = GotDrinksMsg DrinksPage.Msg
    | ClickedReload


update : (Msg -> msg) -> Msg -> State.Model -> Model -> ( Model, Cmd msg )
update toMsg msg state model =
    case ( msg, model.page ) of
        ( GotDrinksMsg drinksMsg, Drinks drinksModel ) ->
            DrinksPage.update (GotDrinksMsg >> toMsg) drinksMsg state drinksModel
                |> Tuple.mapFirst (\newModel -> { model | page = Drinks newModel })

        ( ClickedReload, _ ) ->
            ( model, Nav.reload )

        _ ->
            ( model, Cmd.none )



--
-- VIEW
--


view : (Msg -> msg) -> State.Model -> Model -> Html msg
view toMsg state model =
    case ( model.route, model.page ) of
        ( Start, Static ) ->
            StartPage.view

        ( Favorites, Drinks drinksModel ) ->
            DrinksPage.view (GotDrinksMsg >> toMsg) state drinksModel

        ( Router.Drinks _, Drinks drinksModel ) ->
            DrinksPage.view (GotDrinksMsg >> toMsg) state drinksModel

        ( Impressum, Static ) ->
            ImpressumPage.view

        ( NotFound url, Static ) ->
            NotFoundPage.view url

        _ ->
            -- TODO: Making impossible states impossible
            -- Here, we reached a point where the `route` changed, but we didn't switch to the correct `page` as well, so we don't have the PageModel to render anything...
            InternalErrorPage.view (toMsg ClickedReload)
