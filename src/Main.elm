module Main exposing (main)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Html exposing (Attribute, Html, a, div, span, text)
import Html.Attributes exposing (class)
import Json.Decode as Json exposing (Decoder)
import PWA
import Page
import Router exposing (Route(..))
import State
import Toast
import Transition
import Url exposing (Url)



--
-- MAIN
--


main : Program Json.Value Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , onUrlRequest = Router.onUrlRequest GotRouterMsg
        , onUrlChange = Router.onUrlChange GotRouterMsg
        }



--
-- MODEL
--


pageTransition : Transition.Config
pageTransition =
    { leave =
        Just
            { active = "absolute inset-0 transition transition-slow"
            , from = "fade-out-left-from"
            , to = "fade-out-left-to"
            }
    , enter =
        Just
            { active = "transition transition-slow ease-in-out"
            , from = "fade-in-right-from"
            , to = "fade-in-right-to"
            }
    }


type Flags
    = Flags Json.Value


type alias Model =
    { key : Nav.Key
    , toasts : Toast.Model Msg
    , state : State.Model
    , pwa : PWA.Model
    , page : Transition.Model Page.Model
    , visible : Int
    }


type Msg
    = GotRouterMsg Router.Msg
    | GotToastMsg Toast.Msg
    | GotStateMsg State.Msg
    | GotPwaMsg PWA.Msg
    | GotPageTransitionMsg Transition.Msg
    | GotPageMsg Page.Msg



--
-- INIT
--


init : Json.Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init jsonFlags url navKey =
    let
        route =
            Router.fromUrl url

        flags =
            Json.decodeValue flagsDecoder jsonFlags

        ( state, toasts, cmds ) =
            case flags |> Result.andThen (\(Flags jsonState) -> State.init GotStateMsg jsonState) of
                Ok ( theState, stateCmd ) ->
                    ( theState, Toast.empty, stateCmd )

                Err _ ->
                    let
                        ( theToasts, toastCmd ) =
                            "Deine Favoriten konnten Aufgrund von einem Update leider nicht mehr geladen werden :("
                                |> Toast.notification 15000
                                |> Toast.push GotToastMsg Toast.empty

                        ( theState, stateCmd ) =
                            State.empty GotStateMsg
                    in
                    ( theState, theToasts, Cmd.batch [ toastCmd, stateCmd ] )
    in
    ( { key = navKey
      , toasts = toasts
      , state = state
      , pwa = PWA.init
      , page = Transition.init pageTransition <| Page.fromRoute route state
      , visible = 0
      }
    , cmds
    )


flagsDecoder : Decoder Flags
flagsDecoder =
    Json.map Flags
        (Json.field "state" Json.value)



--
-- UPDATE
--


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotRouterMsg routerMsg ->
            let
                route =
                    Page.route <| Transition.get model.page

                ( newRoute, routerCmds ) =
                    Router.update model.key GotRouterMsg routerMsg route

                ( newPage, pageCmds ) =
                    if newRoute /= route then
                        Transition.to GotPageTransitionMsg (Page.fromRoute newRoute model.state) model.page

                    else
                        ( model.page, Cmd.none )
            in
            ( { model | page = newPage }, Cmd.batch [ routerCmds, pageCmds ] )


        GotToastMsg toastMsg ->
            Toast.update model.toasts toastMsg
                |> Tuple.mapFirst (\toasts -> { model | toasts = toasts })

        GotStateMsg stateMsg ->
            State.update GotStateMsg stateMsg model.state
                |> Tuple.mapFirst (\state -> { model | state = state })


        GotPwaMsg pwaMsg ->
            let
                ( newPwa, pwaCmd, toasts ) =
                    PWA.update GotPwaMsg pwaMsg model.pwa

                ( newToasts, toastCmds ) =
                    Toast.pushAll GotToastMsg model.toasts toasts
            in
            ( { model | pwa = newPwa, toasts = newToasts }
            , Cmd.batch [ pwaCmd, toastCmds ]
            )

        GotPageTransitionMsg transitionMsg ->
            ( { model | page = Transition.update transitionMsg model.page }, Cmd.none )

        GotPageMsg pageMsg ->
            Transition.active (Page.update GotPageMsg pageMsg model.state) model.page
                |> Tuple.mapFirst (\newPage -> { model | page = newPage })


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ PWA.subscriptions GotPwaMsg
        , State.subscriptions GotStateMsg
        ]



--
-- VIEW
--


view : Model -> Document Msg
view model =
    let
        route =
            Page.route <| Transition.get model.page
    in
    { title = title route
    , body =
        [ Router.header GotRouterMsg route
        , Transition.view "div" [ class "flex-1 relative overflow-hidden" ] (viewPage model) GotPageTransitionMsg model.page
        , Toast.view GotToastMsg model.toasts
        ]
    }


viewPage : Model -> List (Attribute Msg) -> Page.Model -> Html Msg
viewPage model attrs page =
    div (class "flex flex-col w-full h-full overflow-y-auto" :: attrs)
        [ Page.view GotPageMsg model.state page
        , footer model
        ]


title : Route -> String
title route =
    if Router.isStart route then
        Router.title route

    else
        Router.title route ++ " | " ++ Router.title Start


footer : Model -> Html msg
footer model =
    Html.footer [ class "flex justify-between mt-auto pt-12 px-3 pb-1 text-xs" ]
        [ span [] [ text "© 2019 Joshua Reusch" ]
        , a [ Router.href Impressum, class "hover:underline" ] [ text "Impressum / Kontakt" ]
        ]
