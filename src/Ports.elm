port module Ports exposing
    ( install
    , installEventRecieved
    , persistState
    , serviceWorkerReady
    , stateUpdated
    , update
    , updateDownloaded
    )

import Json.Encode exposing (Value)



-- Incoming


port serviceWorkerReady : (() -> msg) -> Sub msg


port updateDownloaded : (() -> msg) -> Sub msg


port installEventRecieved : (Value -> msg) -> Sub msg


port stateUpdated : (Value -> msg) -> Sub msg



-- Outgoing


port persistState : Value -> Cmd msg


port update : () -> Cmd msg


port install : Value -> Cmd msg
