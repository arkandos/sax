module Router exposing
    ( Msg
    , Route(..)
    , fromUrl
    , header
    , href
    , icon
    , isStart
    , onUrlChange
    , onUrlRequest
    , title
    , toString
    , update
    )

import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav
import Drinks
import Html exposing (Attribute, Html, button, div, h1, i, text)
import Html.Attributes as Attr exposing (class)
import Html.Events exposing (onClick)
import Url exposing (Url)
import Url.Builder exposing (absolute)
import Url.Parser as Parser exposing (Parser, map, oneOf, s, top)
import Utils



--
-- MODEL
--


type Route
    = Start
    | Favorites
    | Drinks Drinks.Category
    | Impressum
    | NotFound Url


fromUrl : Url -> Route
fromUrl url =
    Parser.parse parser url
        |> Maybe.withDefault (NotFound url)


isStart : Route -> Bool
isStart route =
    case route of
        Start ->
            True

        _ ->
            False


toString : Route -> String
toString route =
    case route of
        Start ->
            absolute [] []

        Favorites ->
            absolute [ "favorites" ] []

        Drinks category ->
            absolute [ category.key ] []

        Impressum ->
            absolute [ "impressum" ] []

        NotFound _ ->
            absolute [ "404" ] []



--
-- UPDATE
--


type Msg
    = LinkClicked UrlRequest
    | BackClicked
    | Goto Url
    | GoBack
    | UrlChanged Url


onUrlRequest : (Msg -> msg) -> (UrlRequest -> msg)
onUrlRequest toMsg =
    LinkClicked >> toMsg


onUrlChange : (Msg -> msg) -> (Url -> msg)
onUrlChange toMsg =
    UrlChanged >> toMsg


update : Nav.Key -> (Msg -> msg) -> Msg -> Route -> ( Route, Cmd msg )
update key toMsg msg route =
    case msg of
        LinkClicked (Internal newUrl) ->
            ( route, Utils.delay 200 (toMsg <| Goto newUrl) )

        LinkClicked (External link) ->
            ( route, Nav.load link )

        BackClicked ->
            ( route, Utils.delay 200 (toMsg GoBack) )

        Goto url ->
            ( route, Nav.pushUrl key (Url.toString url) )

        GoBack ->
            ( route, Nav.back key 1 )

        UrlChanged newUrl ->
            ( fromUrl newUrl, Cmd.none )



--
-- VIEW
--


header : (Msg -> msg) -> Route -> Html msg
header toMsg route =
    Html.header [ class "h-20 flex items-center bg-orange-100 text-orange-900 shadow-lg border-b border-orange-400" ]
        [ if not (isStart route) then
            button [ onClick (toMsg BackClicked), class "self-stretch px-4 transition transition-slow ease-out font-bold text-2xl focus:outline-none focus:bg-orange-200 focus:bg-orange-200" ]
                [ i [ class "i-cheveron-left" ] [] ]

          else
            div [ class "logo" ] []
        , h1 [ class "font-bold text-xl mx-3" ]
            [ text (title route)
            , if not (isStart route) then
                i [ class <| icon route, class "ml-2" ] []

              else
                text ""
            ]
        ]


title : Route -> String
title route =
    case route of
        Start ->
            "Sax with me!"

        Favorites ->
            "Favoriten"

        Drinks category ->
            category.name

        Impressum ->
            "Impressum"

        NotFound _ ->
            "Nicht gefunden!"


icon : Route -> String
icon route =
    case route of
        Start ->
            "i-home"

        Favorites ->
            "i-star"

        Drinks category ->
            category.icon

        Impressum ->
            "i-info-circled"

        NotFound _ ->
            "i-attention"


href : Route -> Attribute msg
href =
    toString >> Attr.href



--
-- HELPER
--


parser : Parser (Route -> a) a
parser =
    oneOf
        [ map Favorites <| s "favorites"
        , map Drinks <|
            oneOf <|
                List.map (\category -> map category <| s category.key) Drinks.categories
        , map Impressum <| s "impressum"
        , map Start <| top
        ]
