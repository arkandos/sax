module PWA exposing (Model, Msg, init, subscriptions, update)

import Json.Decode as Json
import Ports
import Toast exposing (Toast)



-- MODEL


type Model
    = RegisteringServiceWorker
    | ServiceWorkerActive
    | GotInstallEvent Json.Value
    | Prompted Json.Value
    | Requested


init : Model
init =
    RegisteringServiceWorker



-- UPDATE


type Msg
    = ServiceWorkerReady
    | UpdateDownloaded
    | InstallEventRecieved Json.Value
    | UpdateRequested
    | InstallRequested


update : (Msg -> msg) -> Msg -> Model -> ( Model, Cmd msg, List (Toast msg) )
update toMsg msg model =
    case msg of
        ServiceWorkerReady ->
            case model of
                RegisteringServiceWorker ->
                    ( ServiceWorkerActive, Cmd.none, [] )

                GotInstallEvent installEvent ->
                    promptInstall toMsg installEvent

                _ ->
                    ( model, Cmd.none, [] )

        UpdateDownloaded ->
            ( model
            , Cmd.none
            , [ Toast.action "Es ist ein Update verfügbar. Jetzt aktualisieren?" (toMsg UpdateRequested) "Update!" ]
            )

        InstallEventRecieved installEvent ->
            case model of
                RegisteringServiceWorker ->
                    ( GotInstallEvent installEvent, Cmd.none, [] )

                ServiceWorkerActive ->
                    promptInstall toMsg installEvent

                _ ->
                    ( model, Cmd.none, [] )

        UpdateRequested ->
            ( model, Ports.update (), [] )

        InstallRequested ->
            case model of
                Prompted installEvent ->
                    ( Requested, Ports.install installEvent, [] )

                _ ->
                    ( model, Cmd.none, [] )


promptInstall : (Msg -> msg) -> Json.Value -> ( Model, Cmd msg, List (Toast msg) )
promptInstall toMsg installEvent =
    ( Prompted installEvent
    , Cmd.none
    , [ Toast.action "App ist jetzt offline verfügbar!" (toMsg InstallRequested) "Cool!" ]
    )



-- SUBSCRIPTIONS


subscriptions : (Msg -> msg) -> Sub msg
subscriptions toMsg =
    Sub.batch
        [ Ports.serviceWorkerReady (\_ -> toMsg ServiceWorkerReady)
        , Ports.updateDownloaded (\_ -> toMsg UpdateDownloaded)
        , Ports.installEventRecieved (InstallEventRecieved >> toMsg)
        ]
