module Drinks exposing (Category, Drink, byKey, byCategory, categories)

import Dict exposing (Dict)


type alias Key =
    String


type alias Category =
    { key : String
    , name : String
    , icon : String
    }


beer : Category
beer =
    { key = "beer"
    , name = "Bier"
    , icon = "i-pint"
    }


wine : Category
wine =
    { key = "wine"
    , name = "Wein"
    , icon = "i-wine"
    }


longdrinks : Category
longdrinks =
    { key = "longdrinks"
    , name = "Longdrinks"
    , icon = "i-drink"
    }


cocktails : Category
cocktails =
    { key = "cocktails"
    , name = "Cocktails"
    , icon = "i-cocktail"
    }


shots : Category
shots =
    { key = "shots"
    , name = "Kurze"
    , icon = "i-shot"
    }


bottles : Category
bottles =
    { key = "bottles"
    , name = "Flasche Hausschnaps"
    , icon = "i-bottles"
    }


kult : Category
kult =
    { key = "kult"
    , name = "Kult"
    , icon = "i-eskp2"
    }


nonAlcoholic : Category
nonAlcoholic =
    { key = "sodas"
    , name = "Alkoholfreie Getränke"
    , icon = "i-coke"
    }


categories : List Category
categories =
    [ beer, wine, longdrinks, cocktails, shots, kult, bottles, nonAlcoholic ]


type alias Drink =
    { key : Key
    , categories : List Category
    , name : String
    , description : String
    , price : Int
    }


byKey : Key -> Maybe Drink
byKey key =
  Dict.get key all


byCategory : Category -> List Drink
byCategory category =
    List.filter (\drink -> List.member category drink.categories) data


all : Dict Key Drink
all =
    List.foldl (\drink result -> Dict.insert drink.key drink result) Dict.empty data



-- BIG DATA DUMB


data : List Drink
data =
    [ { key = "spaten"
      , categories = [ beer ]
      , name = "Spaten"
      , description = "Lass dir raten, drinke Rahner!"
      , price = 275
      }
    , { key = "rahner_hell"
      , categories = [ beer ]
      , name = "Rahner Helles"
      , description = "Besser, und günstiger!"
      , price = 250
      }
    , { key = "alkfrei_hell"
      , categories = [ beer, nonAlcoholic ]
      , name = "Alkoholfreies Helles"
      , description = "Löwenbräu"
      , price = 175
      }
    , { key = "radler"
      , categories = [ beer ]
      , name = "Radler"
      , description = "Dunkel / Hell"
      , price = 225
      }
    , { key = "keller"
      , categories = [ beer ]
      , name = "Kellerbier"
      , description = "Franziskaner"
      , price = 300
      }
    , { key = "weiss"
      , categories = [ beer ]
      , name = "Weizen"
      , description = "Franziskaner - Dunkel / Hell / Leicht"
      , price = 300
      }
    , { key = "alkfrei_weiss"
      , categories = [ beer, nonAlcoholic ]
      , name = "Alkoholfreies Weizen"
      , description = ""
      , price = 250
      }
    , { key = "weiss_cola"
      , categories = [ beer ]
      , name = "Colaweizen"
      , description = "Mit Pizza für 3,50€"
      , price = 250
      }
    , { key = "weiss_russ"
      , categories = [ beer ]
      , name = "Russen"
      , description = "Mit Limo"
      , price = 250
      }
    , { key = "dunkel"
      , categories = [ beer ]
      , name = "Dunkles Helles"
      , description = "Löwenbräu"
      , price = 275
      }
    , { key = "bock"
      , categories = [ beer ]
      , name = "Triumphator"
      , description = ""
      , price = 325
      }
    , { key = "pils_rhaner"
      , categories = [ beer ]
      , name = "Rhaner Pils"
      , description = "0,33l"
      , price = 250
      }
    , { key = "heineken"
      , categories = [ beer ]
      , name = "Heineken"
      , description = "0,33l"
      , price = 275
      }
    , { key = "becks"
      , categories = [ beer ]
      , name = "Becks Green Lemon"
      , description = "Aber wenn du ein Becks Lemon bist, dann bitte tu mir den gefallen und stell dich nicht zu den anderen Bieren und to so, als würdest du dazugehören, Arschloch!"
      , price = 225
      }
    , { key = "goass"
      , categories = [ beer ]
      , name = "Goass-Halbe"
      , description = ""
      , price = 375
      }
    , { key = "isar"
      , categories = [ beer ]
      , name = "Isarhalbe"
      , description = "Weizen, Blue Curacao, Apfelsaft"
      , price = 375
      }

    -- Wein
    , { key = "wein_weiss"
      , categories = [ wine ]
      , name = "Weißwein"
      , description = "Deutsches Erzeugnis, \"\"\"Qualitätswein\"\"\""
      , price = 250
      }
    , { key = "rotwein"
      , categories = [ wine ]
      , name = "Rotwein"
      , description = "Chilenisches Erzeugnis, \"\"\"Qualitätswein\"\"\""
      , price = 250
      }
    , { key = "fruchtzwerg"
      , categories = [ wine ]
      , name = "Fruchtzwerg"
      , description = "Erdbeere, Heildelbeere, Kirsche"
      , price = 100
      }
    , { key = "wein_schorle"
      , categories = [ wine ]
      , name = "Weißweinschorle"
      , description = ""
      , price = 275
      }
    , { key = "prosi"
      , categories = [ wine, kult ]
      , name = "Prosecco Frizzante"
      , description = "Probieren Sie auch: Mit Erdbeerlimes!"
      , price = 175
      }
    , { key = "aperol"
      , categories = [ wine, cocktails ]
      , name = "Aperol Sprizz"
      , description = "Aperol, Prosecco, Soda, Orange"
      , price = 350
      }
    , { key = "thresh"
      , categories = [ wine ]
      , name = "Laterndl Mass"
      , description = "Weisweinschorle mit Kirsche - Ja das beschreibt es ganz gut"
      , price = 800
      }

    -- Longdrinks
    , { key = "cuba"
      , categories = [ longdrinks, cocktails ]
      , name = "Cuba Libré"
      , description = "Havana Club, Cola, Limette"
      , price = 450
      }
    , { key = "fizz"
      , categories = [ longdrinks, cocktails ]
      , name = "Gin Fizz"
      , description = "Gin, Zitrone, Soda"
      , price = 400
      }
    , { key = "jack"
      , categories = [ longdrinks, kult ]
      , name = "Jack Cola"
      , description = "Auch genannt Benschorle!"
      , price = 500
      }
    , { key = "bacardi"
      , categories = [ longdrinks ]
      , name = "Barcadi Cola"
      , description = ""
      , price = 400
      }
    , { key = "gin_soda"
      , categories = [ longdrinks ]
      , name = "Gin Soda"
      , description = ""
      , price = 350
      }
    , { key = "havana"
      , categories = [ longdrinks ]
      , name = "Havana Cola"
      , description = "Wenn mal die Limetten aus sind!"
      , price = 400
      }
    , { key = "wodka_soda"
      , categories = [ longdrinks ]
      , name = "Wodka Soda"
      , description = "Probieren Sie auch: Mit Kirschsaft!"
      , price = 350
      }


    -- Cocktails
    , { key = "aperol_sour"
      , categories = [ cocktails ]
      , name = "Aperol Sour"
      , description = "Aperol, Zitrone, Lime Juice, Orange, Zucker"
      , price = 400
      }
    , { key = "atw"
      , categories = [ cocktails ]
      , name = "Around the World"
      , description = "Gin, Kirsch, Zitrone, Maracuja, Kirsche"
      , price = 400
      }
    , { key = "bloody_mary"
      , categories = [ cocktails ]
      , name = "Bloody Mary"
      , description = "Wodka, Tomatensaft, Zitrone, Gewürze"
      , price = 500
      }
    , { key = "caipi"
      , categories = [ cocktails ]
      , name = "Caipirinha"
      , description = "Cachaca, Rohrzucker, Limette"
      , price = 500
      }
    , { key = "golden_dream"
      , categories = [ cocktails ]
      , name = "Golden Dream"
      , description = "43er, Apricot Brandy, Orange, Sahne"
      , price = 400
      }
    , { key = "42"
      , categories = [ cocktails ]
      , name = "Donnergurgler"
      , description = "Bourbon, Rum, Grenadine, Zitrone, Orange, Maracuja"
      , price = 600
      }
    , { key = "homerun"
      , categories = [ cocktails ]
      , name = "Homerun"
      , description = "Wodka, Grenadine, Grapefruit, Schwarze Johannisbeere"
      , price = 350
      }
    , { key = "long_island"
      , categories = [ cocktails ]
      , name = "Long Island"
      , description = "Wodka, Gin, Rum, Tequila, Triple Sec, Limette, Cola"
      , price = 600
      }
    , { key = "pink"
      , categories = [ cocktails ]
      , name = "Mister Pink"
      , description = "Wodka, Erdbeerlimes, Grenadine, Maracuja, Sahne"
      , price = 400
      }
    , { key = "pina_colada"
      , name = "Pina Colada"
      , categories = [ cocktails ]
      , description = "Rum, Kokos, Ananas, Sahne"
      , price = 400
      }
    , { key = "sotb"
      , categories = [ cocktails ]
      , name = "Sex on the Beach"
      , description = "Wodka, Pfirsich, Grenadine, Zitrone, Limette, Ananas"
      , price = 400
      }
    , { key = "ssling"
      , categories = [ cocktails ]
      , name = "Singapur Sling"
      , description = "Gin, Kirsche, Triple Sec, Angost., Grenadine, Limette, Ananas"
      , price = 500
      }
    , { key = "solero"
      , categories = [ cocktails ]
      , name = "Solero"
      , description = "Wodka, Vanille, Orange, Maracuja"
      , price = 350
      }
    , { key = "tsunrise"
      , categories = [ cocktails ]
      , name = "Tequila Sunrise"
      , description = "Tequila, Grenadine, Limette, Orange"
      , price = 400
      }
    , { key = "touchdown"
      , categories = [ cocktails ]
      , name = "Touchdown"
      , description = "Wodka, Apricot Brandy, Grenadine, Zitrone, Maracuja"
      , price = 350
      }
    , { key = "wr"
      , categories = [ cocktails ]
      , name = "White Russian"
      , description = "Wodka, Kaffeelikeur, Sahne"
      , price = 500
      }

    -- Shots
    , { key = "apfelstrudel"
      , categories = [ shots ]
      , name = "Apfelstrudel"
      , description = "Malibu, Apfelsaft, Zimt"
      , price = 225
      }
    , { key = "anton"
      , categories = [ shots ]
      , name = "Anton"
      , description = "Wodka mit Almdudler"
      , price = 175
      }
    , { key = "mmilch"
      , categories = [ shots ]
      , name = "Mäusemilch"
      , description = "Korn, Vanille, Milch"
      , price = 175
      }
    , { key = "mofa"
      , categories = [ shots ]
      , name = "Mofa"
      , description = "Wodka, Fanta"
      , price = 175
      }
    , { key = "pfeffi_bull"
      , categories = [ shots ]
      , name = "Pfeffi Bull"
      , description = "Pfeffi, Red Bull"
      , price = 225
      }
    , { key = "wopfl"
      , categories = [ shots ]
      , name = "Wopfl"
      , description = "Wodka, Apfelsaft"
      , price = 175
      }
    , { key = "tex"
      , categories = [ shots ]
      , name = "Tex"
      , description = "Probieren Sie besser nicht: Mit Ananas!"
      , price = 125
      }
    , { key = "b52"
      , categories = [ shots ]
      , name = "B52"
      , description = "Auch mit Bier sehr beliebt!"
      , price = 325
      }
    , { key = "b53"
      , categories = [ shots ]
      , name = "Baseball"
      , description = "73er, Escorial"
      , price = 325
      }
    , { key = "bomb"
      , categories = [ shots ]
      , name = "Time Bomb"
      , description = "Sambuca, Tequila, Tabasco"
      , price = 275
      }
    , { key = "toothpaste"
      , categories = [ shots ]
      , name = "Peffi"
      , description = "Was? Nur 4,50€?!"
      , price = 75
      }
    , { key = "billa"
      , categories = [ shots ]
      , name = "Hausschnaps"
      , description = "Was für Sorten es gibt hab ich gfragt!"
      , price = 75
      }
    , { key = "mex"
      , categories = [ shots ]
      , name = "Mexikaner"
      , description = "\"\"\"Hausgemacht\"\"\""
      , price = 200
      }
    , { key = "jaeger"
      , categories = [ shots ]
      , name = "Jägermeister"
      , description = "Kotzband an mich bitte!"
      , price = 175
      }
    , { key = "ouzo"
      , categories = [ shots ]
      , name = "Ouzo"
      , description = "Probieren Sie besser nicht: Mit Almdudler!"
      , price = 150
      }
    , { key = "obstler"
      , categories = [ shots ]
      , name = "Obstler"
      , description = "Um nach 12 Weizen mal \"den Magen zu beruhigen\""
      , price = 150
      }
    , { key = "worst"
      , categories = [ kult ]
      , name = "DER DOUZLER"
      , description = "DER BESTE KURZE IM LANDKREIS!"
      , price = 100
      }
    , { key = "2nd_worst"
      , categories = [ kult ]
      , name = "Tex-Ananas"
      , description = "Beim Rainer hätte es so etwas nicht gegeben!"
      , price = 100
      }

    -- Non-Alcoholic
    , { key = "gas"
      , categories = [ nonAlcoholic ]
      , name = "Wasser"
      , description = "Mit Kohlensäure, 0,4l"
      , price = 175
      }
    , { key = "juice"
      , categories = [ nonAlcoholic ]
      , name = "Säfte / Nektar"
      , description = "Ananas, Apfel, Orange, Kiba, Grapefruit, Johannis., Kirsche, Maracuja"
      , price = 300
      }
    , { key = "spritzer"
      , categories = [ nonAlcoholic ]
      , name = "Saftschorle"
      , description = "Ananas, Apfel, Orange, Kiba, Grapefruit, Johannis., Kirsche, Maracuja"
      , price = 250
      }
    , { key = "soda"
      , categories = [ nonAlcoholic ]
      , name = "Limonade"
      , description = "Almdudler, Bitter Lemon, Cola, Cola Light, Fanta, Ginger Ale, Spezi, Weisse Limo, Tonic Water"
      , price = 200
      }
    , { key = "icetea"
      , categories = [ nonAlcoholic ]
      , name = "Eistee"
      , description = "0,49€ vom Baumann, Pfirsich oder Zitrone"
      , price = 175
      }
    , { key = "redbull"
      , categories = [ nonAlcoholic ]
      , name = "Red Bull"
      , description = "Verleiht die Kraft, noch ins Zap zu kommen"
      , price = 350
      }

    --
    ]
