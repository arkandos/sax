module Pages.InternalError exposing (view)

import Html exposing (Html, br, div, h2, main_, p, text)
import Html.Attributes exposing (class)
import Widgets.Button as Button


view : msg -> Html msg
view reload =
    main_ [ class "mx-3 mt-8 font-base text-white leading-wide" ]
        [ h2 [ class "text-lg font-semibold mt-2" ] [ text "Interner Fehler" ]
        , p [ class "mt-4" ]
            [ text "Es ist ein interner Logikfehler aufgetreten, der eigentlich niemals passieren können sollte."
            , br [] []
            , text "Solltest du mit aller Wahrscheinlichkeit diese Nachricht trotzdem eines Tages sehen, schuldet dir der Entwickler ein Bier."
            ]
        , p [ class "mt-4" ]
            [ text "Du kannst für den Moment versuchen, die App neuzuladen (wie in Chrome von oben ziehen)."
            ]
        , div [ class "mt-8 flex justify-center" ]
            [ Button.default [] reload "i-arrows-cw" "Seite neu laden"
            ]
        ]
