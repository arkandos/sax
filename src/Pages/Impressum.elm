module Pages.Impressum exposing (view)

import Html exposing (Html, address, br, h2, h3, main_, p, text)
import Html.Attributes exposing (class)


view : Html msg
view =
    main_ [ class "mx-3 mt-8 font-base text-white leading-wide" ]
        [ h2 [ class "text-3xl leading-tight font-bold" ] [ text "Impressum" ]
        , p [ class "mt-2" ] [ text "Angaben gemäß §5 TMG" ]
        , address [ class "mt-4" ]
            [ text "Joshua Reusch"
            , br [] []
            , text "Hemauerstraße 20"
            , br [] []
            , text "93047 Regensburg"
            ]
        , h3 [ class "mt-6 text-xl leading-tight font-semibold" ] [ text "Vertreten durch" ]
        , p [ class "mt-2" ] [ text "Joshua Reusch" ]
        , h3 [ class "mt-6 text-xl leading-tight font-semibold" ] [ text "Kontakt" ]
        , p [ class "mt-2" ]
            [ text "Telefon: 0176 / 64 388 792"
            , br [] []
            , text "E-Mail: jreusch@orange-circle.de"
            ]
        , h3 [ class "mt-6 text-xl leading-tight font-semibold" ] [ text "Datenschutz" ]
        , p [ class "mt-2" ]
            [ text "Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben."
            ]
        , p [ class "mt-4" ]
            [ text "Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich."
            ]
        , p [ class "mt-4" ]
            [ text "Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor."
            ]
        ]
