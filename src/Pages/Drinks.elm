module Pages.Drinks exposing (Model, Msg, initForCategory, initForFavorites, update, view)

import Dict.Any as AnyDict exposing (AnyDict)
import Drinks exposing (Category, Drink)
import Html exposing (Attribute, Html, button, div, i, span, text)
import Html.Attributes exposing (class)
import Html.Events as Events exposing (onClick)
import Html.Keyed as Keyed
import Json.Decode as Json
import Process
import State
import Task
import Time exposing (Posix)
import Transition
import Widgets.Button as Button



--
-- CONFIG
--


editDismissDelay : Int
editDismissDelay =
    5000


transitionConfig : Transition.Config
transitionConfig =
    { enter = Nothing
    , leave =
        Just
            { active = "block absolute right-0 transition transition-slowest ease-out"
            , from = "fade-up-from"
            , to = "fade-up-to"
            }
    }



--
-- INIT
--


type alias Model =
    { drinks : List Drink
    , editing : Maybe Drink
    , transitions : AnyDict String Drink (Transition.Model Int)
    }


type Msg
    = GotTransitionMsg Drink Transition.Msg
    | ClickedFavoriteButton Drink
    | ClickedUpdateButton Drink Int
    | DrinkLongPressed Drink
    | EditTimeoutElapsed Drink Posix


initForCategory : Category -> Model
initForCategory category =
    initForDrinks <| Drinks.byCategory category


initForFavorites : State.Model -> Model
initForFavorites state =
    initForDrinks <| State.favoritesAndSelected state


initForDrinks : List Drink -> Model
initForDrinks drinks =
    { editing = Nothing
    , drinks = drinks
    , transitions = AnyDict.empty .key
    }



--
-- UPDATE
--


update : (Msg -> msg) -> Msg -> State.Model -> Model -> ( Model, Cmd msg )
update toMsg msg state model =
    case msg of
        GotTransitionMsg drink transitionMsg ->
            ( { model
                | transitions =
                    AnyDict.update drink
                        (Maybe.map (Transition.update transitionMsg))
                        model.transitions
              }
            , Cmd.none
            )

        DrinkLongPressed drink ->
            ( { model | editing = Just drink }
            , checkEditTimer toMsg drink
            )

        ClickedFavoriteButton drink ->
            ( { model | editing = Nothing }
            , State.persist <| State.toggleFavorite drink state
            )

        ClickedUpdateButton drink diff ->
            let
                newState =
                    State.updateCount drink diff state

                ( transition, transitionCmd ) =
                    AnyDict.get drink model.transitions
                        |> Maybe.withDefault (Transition.init transitionConfig <| State.count drink state)
                        |> Transition.to (GotTransitionMsg drink >> toMsg) (State.count drink newState)

                newTransitions =
                    AnyDict.insert drink transition model.transitions
            in
            if Just drink == model.editing then
                ( { model | transitions = newTransitions }
                , Cmd.batch
                    [ State.persist newState
                    , checkEditTimer toMsg drink
                    , transitionCmd
                    ]
                )

            else
                ( { model | editing = Nothing, transitions = newTransitions }
                , Cmd.batch
                    [ State.persist newState
                    , transitionCmd
                    ]
                )

        EditTimeoutElapsed drink time ->
            let
                isEditingDrink =
                    Just drink == model.editing

                enoughTimeElapsed =
                    editDismissDelay <= Time.posixToMillis time - Time.posixToMillis (State.lastUpdate state)
            in
            if isEditingDrink && enoughTimeElapsed then
                ( { model | editing = Nothing }, Cmd.none )

            else
                ( model, Cmd.none )


checkEditTimer : (Msg -> msg) -> Drink -> Cmd msg
checkEditTimer toMsg drink =
    Process.sleep (toFloat editDismissDelay)
        |> Task.andThen (always Time.now)
        |> Task.perform (EditTimeoutElapsed drink >> toMsg)



--
-- VIEW
--


view : (Msg -> msg) -> State.Model -> Model -> Html msg
view toMsg state model =
    model.drinks
        |> List.map (\drink -> ( drink.key, viewDrink toMsg state model drink ))
        |> Keyed.node "main" [ class "mt-4" ]


viewDrink : (Msg -> msg) -> State.Model -> Model -> Drink -> Html msg
viewDrink toMsg state model drink =
    let
        count =
            State.count drink state

        isFavorite =
            State.isFavorite drink state

        transition =
            AnyDict.get drink model.transitions
                |> Maybe.withDefault (Transition.init transitionConfig count)
    in
    case model.editing of
        Just editingDrink ->
            if editingDrink == drink then
                viewDrinkEditing toMsg transition drink

            else
                viewDrinkDefault toMsg transition isFavorite drink

        Nothing ->
            viewDrinkDefault toMsg transition isFavorite drink


viewDrinkEditing : (Msg -> msg) -> Transition.Model Int -> Drink -> Html msg
viewDrinkEditing toMsg count drink =
    div [ class "shadow-inner bg-orange-800" ]
        [ div [ class "flex items-center py-6 mx-6" ]
            [ Button.default [ class "flex-none h-10" ] (toMsg <| ClickedUpdateButton drink -5) "i-minus-thick" "5"
            , Button.default [ class "flex-none h-10 ml-4" ] (toMsg <| ClickedUpdateButton drink -1) "i-minus-thick" "1"
            , viewCount [ class "mx-auto" ] toMsg drink count
            , Button.default [ class "flex-none h-10" ] (toMsg <| ClickedUpdateButton drink 1) "i-plus-thick" "1"
            , Button.default [ class "flex-none h-10 ml-4" ] (toMsg <| ClickedUpdateButton drink 5) "i-plus-thick" "5"
            ]
        ]


viewDrinkDefault : (Msg -> msg) -> Transition.Model Int -> Bool -> Drink -> Html msg
viewDrinkDefault toMsg count isFavorite drink =
    div
        [ onLongPress (toMsg <| DrinkLongPressed drink)
        , class "transition transition-slowest ease-out active:shadow-inner active:bg-orange-800"
        ]
        [ div [ class "flex py-6 mx-6 border-b border-orange-600" ]
            [ Button.default [ class "flex-none h-10" ] (toMsg <| ClickedUpdateButton drink 1) "i-plus-thick" "1"
            , div [ class "mx-4 leading-tight" ]
                [ div [ class "text-lg" ] [ text drink.name ]
                , div [ class "text-sm italic text-orange-300" ] [ text drink.description ]
                ]
            , viewCount [ class "ml-auto" ] toMsg drink count
            , button
                [ onClick (toMsg <| ClickedFavoriteButton drink)
                , class "flex-none self-center h-10 w-10 ml-6 text-2xl bg-orange-300 rounded-full shadow transition transition-fast ease-in-out active:bg-orange-800 active:text-orange-200 focus:outline-none"
                , if isFavorite then
                    class "bg-orange-800 text-orange-200 active:bg-orange-300 active:text-orange-800"

                  else
                    class "bg-orange-300 active:bg-orange-800 active:text-orange-200"
                ]
                [ i [ class "i-star" ] [] ]
            ]
        ]


viewCount : List (Attribute msg) -> (Msg -> msg) -> Drink -> Transition.Model Int -> Html msg
viewCount attrs toMsg drink =
    Transition.view "div"
        (class "relative self-center leading-none text-4xl font-semibold text-orange-200" :: attrs)
        (\attr count -> span attr [ text <| String.fromInt count ])
        (GotTransitionMsg drink >> toMsg)


onLongPress : msg -> Attribute msg
onLongPress msg =
    Events.on "long-press" (Json.succeed msg)
