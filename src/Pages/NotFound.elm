module Pages.NotFound exposing (view)

import Html exposing (Html, a, div, em, h2, main_, p, text)
import Html.Attributes exposing (class)
import Router exposing (Route(..))
import Url exposing (Url)
import Widgets.Button as Button


view : Url -> Html msg
view url =
    main_ [ class "mx-3 mt-8 font-base text-white leading-wide" ]
        [ h2 [ class "text-lg font-semibold mt-2" ] [ text "Fehler 404" ]
        , p [ class "mt-4" ]
            [ text "Unter der aufgerufenen URL "
            , em [] [ text <| Url.toString url ]
            , text " konnte keine Seite gefunden werden!"
            ]
        , p [ class "mt-4" ]
            [ text "Obwohl diese App so tut, als ob wäre sie eine native Smartphone-App, ist sie in Wirklichkeit einfach eine "
            , em [] [ text "sehr fancy Webseite" ]
            , text ", mit URLs, und Fehler 404, und allem drum und dran!"
            ]
        , p [ class "mt-4" ]
            [ text "Wenn du zum Beispiel immer direkt bei deinen Favoriten landen möchtest, kannst du einfach "
            , a [ class "text-orange-200 underline", Router.href Favorites ] [ text "diesen Link hier" ]
            , text " als Lesezeichen hinzufügen."
            ]
        , p [ class "mt-4" ]
            [ text "Leider heißt das aber auch, dass immer wenn sich eine URL ändert, intern ein Link falsch gesetzt ist, oder schlicht Blödsinn in die URL eingegeben wird, diese Seite angezeigt wird, mit einem hilfreichen Button:"
            ]
        , div [ class "mt-8 flex justify-center" ]
            [ Button.defaultLink [] Start "i-home" "Zurück zur Startseite"
            ]
        ]
