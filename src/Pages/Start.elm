module Pages.Start exposing (view)

import Drinks
import Html exposing (Html, a, div, i, main_, text)
import Html.Attributes as Attr exposing (class)
import Router exposing (Route(..))


view : Html msg
view =
    Drinks.categories
        |> List.map (\category -> tab <| Drinks category)
        |> (::) (tabHorizontal Favorites)
        |> main_ [ class "mt-12 mx-3 flex flex-wrap" ]


tab : Route -> Html msg
tab route =
    let
        label =
            Router.title route

        iconName =
            Router.icon route
    in
    div [ class "w-1/2 p-3" ]
        [ a
            [ Router.href route
            , Attr.title label
            , class "block w-full h-full p-3 transition transition-slow ease-out bg-orange-400 rounded-lg shadow-md leading-tight text-center focus:outline-none focus:bg-orange-800"
            ]
            [ i [ class iconName, class "block text-6xl" ] []
            , div [ class "mt-2" ] [ text label ]
            ]
        ]


tabHorizontal : Route -> Html msg
tabHorizontal route =
    let
        label =
            Router.title route

        iconName =
            Router.icon route
    in
    div [ class "w-full p-3" ]
        [ a
            [ Router.href route
            , Attr.title label
            , class "flex items-center w-full h-full py-6 px-10 transition transition-slow ease-out bg-orange-200 text-orange-800 rounded-lg shadow-md leading-tight focus:outline-none focus:bg-orange-400"
            ]
            [ i [ class iconName, class "block text-5xl" ] []
            , div [ class "ml-6 text-xl font-semibold" ] [ text label ]
            ]
        ]
