import { register, unregister } from 'register-service-worker'
import { Elm } from './Main.elm'


import 'long-press-event/dist/long-press-event.min.js'


let initialState = {
    favorites : [],
    counts : {}
}

const storageState = localStorage.getItem('state')
if (storageState) {
    try {
        initialState = JSON.parse(storageState)
    } catch (e) {
        initialState = null
    }
}


const app = Elm.Main.init ({
    node: document.getElementById('app'),
    flags: {
        state: initialState
    }
})


//
// Storage API
//


app.ports.persistState.subscribe(function (newState) {
    newState.lastUpdate = Date.now()
    localStorage.setItem('state', JSON.stringify(newState))
    app.ports.stateUpdated.send (newState)
})


window.addEventListener ('storage', function (e) {
    if (e.key === 'state') {
        app.ports.stateUpdated.send (JSON.parse (e.newValue))
    }
})


//
// BeforeInstallPromptEvent
//


window.addEventListener('beforeinstallprompt', function (e) {
    e.preventDefault ()
    app.ports.installEventRecieved.send(e)
})


app.ports.install.subscribe (function (installPrompt) {
    installPrompt.prompt()
})


//
// ServiceWorker
//


app.ports.update.subscribe (function () {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.ready
            .then(registration => registration.unregister())
            .then(_ => window.location.reload(true))
    } else {
        window.location.reload(true)
    }
})


register('/service-worker.js', {
    registrationOptions: { scope: './' },
    cached(registration) {
        app.ports.serviceWorkerReady.send(null)
    },
    updated(registration) {
        app.ports.updateDownloaded.send(null)
    }
})
