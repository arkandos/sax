module Transition exposing (Config, Model, Msg, active, get, init, set, to, update, view, with)

import Browser.Dom exposing (getViewport)
import Html exposing (Attribute, Html)
import Html.Attributes exposing (class)
import Html.Events exposing (on)
import Html.Keyed as Keyed
import Json.Decode as Json exposing (Decoder)
import Task



--
-- CONFIG
--


type alias Transition =
    { active : String
    , from : String
    , to : String
    }


type alias Config =
    { enter : Maybe Transition
    , leave : Maybe Transition
    }



--
-- MODEL
--


type Model model
    = Model
        { enter : Maybe Transition
        , leave : Maybe Transition
        , activeItem : ActiveItem model
        , leavingItems : List (LeavingItem model)
        , id : Int
        }


type alias ActiveItem model =
    { state : ActiveState
    , model : model
    , id : Int
    }


type ActiveState
    = EnterStart
    | Entering
    | Active


type alias LeavingItem model =
    { state : LeavingState
    , model : model
    , id : Int
    }


type LeavingState
    = LeaveStart
    | Leaving



--
-- INIT
--


init : Config -> model -> Model model
init { enter, leave } activeModel =
    Model
        { enter = enter
        , leave = leave
        , activeItem =
            { state = Active
            , model = activeModel
            , id = 0
            }
        , leavingItems = []
        , id = 1
        }



--
-- QUERY / MODIFY
--


get : Model model -> model
get (Model model) =
    model.activeItem.model


set : model -> Model model -> Model model
set newActiveModel (Model model) =
    let
        currentActive =
            model.activeItem
    in
    Model
        { model | activeItem = { currentActive | model = newActiveModel } }


active : (model -> ( model, Cmd msg )) -> Model model -> ( Model model, Cmd msg )
active doUpdate (Model model) =
    let
        ( newActive, cmd ) =
            doUpdate model.activeItem.model
    in
    ( set newActive (Model model), cmd )


with : (Msg -> msg) -> (model -> ( model, Cmd msg )) -> Model model -> ( Model model, Cmd msg )
with toMsg doUpdate model =
    let
        ( newActive, cmd ) =
            doUpdate <| get model
    in
    to toMsg newActive model
        |> Tuple.mapSecond (\transitionCmd -> Cmd.batch [ transitionCmd, cmd ])


to : (Msg -> msg) -> model -> Model model -> ( Model model, Cmd msg )
to toMsg newModel (Model model) =
    if newModel /= model.activeItem.model then
        ( Model
            { model
                | activeItem = initActive model.enter model.id newModel
                , leavingItems = addLeaving model.leave model.activeItem model.leavingItems
                , id = model.id + 1
            }
        , nextAnimationFrame (toMsg Started)
        )

    else
        ( Model model, Cmd.none )


initActive : Maybe Transition -> Int -> model -> ActiveItem model
initActive config id activeModel =
    { state =
        case config of
            Just _ ->
                EnterStart

            Nothing ->
                Active
    , model = activeModel
    , id = id
    }


addLeaving : Maybe Transition -> ActiveItem model -> List (LeavingItem model) -> List (LeavingItem model)
addLeaving config activeItem leaving =
    case ( activeItem.state, config ) of
        ( Active, Just _ ) ->
            let
                leavingItem =
                    { state = LeaveStart
                    , model = activeItem.model
                    , id = activeItem.id
                    }
            in
            leavingItem :: leaving

        _ ->
            leaving



--
-- UPDATE
--


type Msg
    = Started
    | Entered Int
    | Leaved Int


update : Msg -> Model model -> Model model
update msg (Model model) =
    case msg of
        Started ->
            Model
                { model
                    | activeItem = startedActive model.activeItem
                    , leavingItems = List.map startedLeaving model.leavingItems
                }

        Entered id ->
            let
                currentActive =
                    model.activeItem
            in
            if currentActive.id == id then
                Model
                    { model | activeItem = { currentActive | state = Active } }

            else
                Model model

        Leaved id ->
            Model
                { model | leavingItems = List.filter (\item -> item.id /= id) model.leavingItems }


startedActive : ActiveItem model -> ActiveItem model
startedActive item =
    case item.state of
        EnterStart ->
            { item | state = Entering }

        _ ->
            item


startedLeaving : LeavingItem model -> LeavingItem model
startedLeaving item =
    case item.state of
        LeaveStart ->
            { item | state = Leaving }

        _ ->
            item



--
-- VIEW
--


view :
    String
    -> List (Attribute msg)
    -> (List (Attribute msg) -> model -> Html msg)
    -> (Msg -> msg)
    -> Model model
    -> Html msg
view tagName attrs viewItem toMsg (Model model) =
    model.leavingItems
        |> List.map (viewLeaving viewItem toMsg model.leave)
        |> (::) (viewActive viewItem toMsg model.enter model.activeItem)
        |> List.reverse
        |> Keyed.node tagName attrs


viewActive :
    (List (Attribute msg) -> model -> Html msg)
    -> (Msg -> msg)
    -> Maybe Transition
    -> ActiveItem model
    -> ( String, Html msg )
viewActive viewItem toMsg config item =
    withId item <|
        case ( item.state, config ) of
            ( EnterStart, Just transition ) ->
                viewItem [ class transition.active, class transition.from ] item.model

            ( Entering, Just transition ) ->
                viewItem
                    [ class transition.active
                    , class transition.to
                    , onTransitionEnd (toMsg <| Entered item.id)
                    ]
                    item.model

            _ ->
                viewItem [] item.model


viewLeaving :
    (List (Attribute msg) -> model -> Html msg)
    -> (Msg -> msg)
    -> Maybe Transition
    -> LeavingItem model
    -> ( String, Html msg )
viewLeaving viewItem toMsg config item =
    withId item <|
        case ( item.state, config ) of
            ( LeaveStart, Just transition ) ->
                viewItem [ class transition.active, class transition.from ] item.model

            ( Leaving, Just transition ) ->
                viewItem
                    [ class transition.active
                    , class transition.to
                    , onTransitionEnd (toMsg <| Leaved item.id)
                    ]
                    item.model

            _ ->
                viewItem [] item.model


withId : { r | id : Int } -> Html msg -> ( String, Html msg )
withId { id } html =
    ( String.fromInt id, html )



--
-- HELPERS
--


self : Decoder msg -> Decoder msg
self decoder =
    let
        targetDecoder =
            Json.map2 Tuple.pair
                (Json.field "target" Json.value)
                (Json.field "currentTarget" Json.value)

        isTargetSelf ( target, currentTarget ) =
            if target == currentTarget then
                decoder

            else
                Json.fail "Event only targets the element itself"
    in
    Json.andThen isTargetSelf targetDecoder


onTransitionEnd : msg -> Attribute msg
onTransitionEnd msg =
    on "transitionend" <| self <| Json.succeed msg


nextAnimationFrame : msg -> Cmd msg
nextAnimationFrame msg =
    Task.perform (always msg) getViewport
